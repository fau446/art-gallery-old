export const BASE_URL = 'https://art-gallery-sfu.onrender.com';

export const checkResponse = (res) => (res.ok ? res.json() : res.json()
  .then((err) => Promise.reject(err)));

export const getComments = async (id) => checkResponse(await fetch(`${BASE_URL}/arts/${id}/comments`));

export const addComment = async (id, comment) => {
  const res = await fetch(`${BASE_URL}/arts/${id}/comments`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(comment),
  });
  return checkResponse(res);
};

export const removeComment = async (id, commentId) => {
  const res = await fetch(`${BASE_URL}/arts/${id}/comments/${commentId}`, {
    method: 'DELETE',
  });
  return checkResponse(res);
};

export const login = async (account) => {
  const res = await fetch(`${BASE_URL}/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(account),
  });
  return checkResponse(res);
};

export const fetchAccount = async (username) => {
  const res = await fetch(`${BASE_URL}/account/${username}`);
  return checkResponse(res);
};
