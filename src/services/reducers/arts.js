import {
  GET_ARTS__REQUEST,
  GET_ARTS__SUCCESS,
  GET_ARTS__FAILURE,
  GET_ART_BY_ID__REQUEST,
  GET_ART_BY_ID__SUCCESS,
  GET_ART_BY_ID__FAILURE,
  ADD_ART__REQUEST,
  ADD_ART__SUCCESS,
  ADD_ART__FAILURE,
  REMOVE_ART__REQUEST,
  REMOVE_ART__SUCCESS,
  REMOVE_ART__FAILURE,
  INCREMENT_VIEWS,
  INCREMENT_LIKES,
  DECREMENT_LIKES,
  RESET_ERROR,
  GET_COMMENTS_VIEWS__FAILURE,
  GET_COMMENTS_VIEWS__REQUEST,
  GET_COMMENTS_VIEWS__SUCCESS,
} from '../actions/arts';

const initialState = {
  arts: [],
  modalArt: {},
  loading: false,
  error: null,
};

// eslint-disable-next-line default-param-last
export default function artsReducer(state = initialState, action) {
  switch (action.type) {
    case RESET_ERROR:
      return {
        ...state,
        error: null,
      };
    case GET_COMMENTS_VIEWS__REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case GET_COMMENTS_VIEWS__SUCCESS:
      return {
        ...state,
        modalArt: {
          ...state.modalArt,
          comments: action.payload.comments,
        },
        loading: false,
      };
    case GET_COMMENTS_VIEWS__FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case INCREMENT_VIEWS:
      return {
        ...state,
        arts: state.arts.map((art) => {
          if (art.ID === action.payload) {
            return {
              ...art,
              views: art.views + 1,
            };
          }
          return art;
        }),
      };
    case INCREMENT_LIKES:
      return {
        ...state,
        arts: state.arts.map((art) => {
          if (art.ID === action.payload) {
            return {
              ...art,
              likes: art.likes + 1,
            };
          }
          return art;
        }),
      };
    case DECREMENT_LIKES:
      return {
        ...state,
        arts: state.arts.map((art) => {
          if (art.ID === action.payload) {
            return {
              ...art,
              likes: art.likes - 1,
            };
          }
          return art;
        }),
      };
    case GET_ARTS__REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case GET_ARTS__SUCCESS:
      return {
        ...state,
        arts: action.payload,
        loading: false,
      };
    case GET_ARTS__FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case GET_ART_BY_ID__REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case GET_ART_BY_ID__SUCCESS:
      return {
        ...state,
        modalArt: action.payload,
        arts: state.arts.map((art) => {
          if (art.id === action.payload.id) {
            return action.payload;
          }
          return art;
        }),
        loading: false,
      };
    case GET_ART_BY_ID__FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case ADD_ART__REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case ADD_ART__SUCCESS:
      return {
        ...state,
        arts: [...state.arts, action.payload],
      };
    case ADD_ART__FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case REMOVE_ART__REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case REMOVE_ART__SUCCESS:
      return {
        ...state,
        arts: state.arts.filter((art) => art.id !== action.payload.id),
      };
    case REMOVE_ART__FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}
