import {
  CREATE_ACCOUNT__REQUEST,
  CREATE_ACCOUNT__FAILURE,
  CREATE_ACCOUNT__SUCCESS,
  LOGIN__REQUEST,
  LOGIN__FAILURE,
  LOGIN__SUCCESS,
  EXIT_SIGNUP,
  EXIT_LOGIN,
  FETCH_ACCOUNT__SUCCESS,
  FETCH_ACCOUNT__FAILURE,
  FETCH_ACCOUNT__REQUEST,
  ADD_VIEWS__REQUEST,
  ADD_VIEWS__SUCCESS,
  ADD_VIEWS__FAILURE,
  ADD_LIKE__SUCCESS,
  ADD_LIKE__FAILURE,
  ADD_LIKE__REQUEST,
  REMOVE_LIKE__REQUEST,
  REMOVE_LIKE__SUCCESS,
  REMOVE_LIKE__FAILURE,
  LOGOUT,
} from '../actions/account';

const initialState = {
  accountData: {},
  accountCreated: false,
  loggedIn: false,
  loading: false,
  error: null,
};

// eslint-disable-next-line default-param-last
export default function accountReducer(state = initialState, action) {
  switch (action.type) {
    case LOGOUT:
      return {
        accountData: {},
        accountCreated: false,
        loggedIn: false,
        loading: false,
        error: null,
      };
    case CREATE_ACCOUNT__REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case CREATE_ACCOUNT__SUCCESS:
      return {
        ...state,
        accountCreated: true,
        loading: false,
      };
    case CREATE_ACCOUNT__FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case LOGIN__REQUEST:
      return {
        ...state,
        loggedIn: false,
        loading: true,
        error: null,
      };
    case LOGIN__SUCCESS:
      return {
        ...state,
        loggedIn: true,
        loading: false,
      };
    case LOGIN__FAILURE:
      return {
        ...state,
        loading: false,
        loggedIn: false,
        error: action.payload,
      };
    case EXIT_SIGNUP:
      return {
        ...state,
        accountCreated: false,
      };
    case EXIT_LOGIN:
      return {
        ...state,
      };
    case FETCH_ACCOUNT__REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_ACCOUNT__SUCCESS:
      return {
        ...state,
        accountData: action.payload,
        loading: false,
      };
    case FETCH_ACCOUNT__FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case ADD_VIEWS__REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case ADD_VIEWS__SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case ADD_VIEWS__FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case ADD_LIKE__REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case ADD_LIKE__SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case ADD_LIKE__FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case REMOVE_LIKE__REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case REMOVE_LIKE__SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case REMOVE_LIKE__FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}
