import request from '../../utils/apiUtils';

export const CREATE_ACCOUNT__REQUEST = 'CREATE_ACCOUNT__REQUEST';
export const CREATE_ACCOUNT__SUCCESS = 'CREATE_ACCOUNT__SUCCESS';
export const CREATE_ACCOUNT__FAILURE = 'CREATE_ACCOUNT__FAILURE';
export const EXIT_SIGNUP = 'EXIT_SIGNUP';
export const EXIT_LOGIN = 'EXIT_LOGIN';

export const LOGIN__REQUEST = 'LOGIN__REQUEST';
export const LOGIN__SUCCESS = 'LOGIN__SUCCESS';
export const LOGIN__FAILURE = 'LOGIN__FAILURE';

export const FETCH_ACCOUNT__REQUEST = 'FETCH_ACCOUNT__REQUEST';
export const FETCH_ACCOUNT__SUCCESS = 'FETCH_ACCOUNT__SUCCESS';
export const FETCH_ACCOUNT__FAILURE = 'FETCH_ACCOUNT__FAILURE';

export const ADD_VIEWS__REQUEST = 'ADD_VIEWS__REQUEST';
export const ADD_VIEWS__SUCCESS = 'ADD_VIEWS__SUCCESS';
export const ADD_VIEWS__FAILURE = 'ADD_VIEWS__FAILURE';

export const ADD_LIKE__REQUEST = 'ADD_LIKE__REQUEST';
export const ADD_LIKE__SUCCESS = 'ADD_LIKE__SUCCESS';
export const ADD_LIKE__FAILURE = 'ADD_LIKE__FAILURE';

export const REMOVE_LIKE__REQUEST = 'REMOVE_LIKE__REQUEST';
export const REMOVE_LIKE__SUCCESS = 'REMOVE_LIKE__SUCCESS';
export const REMOVE_LIKE__FAILURE = 'REMOVE_LIKE__FAILURE';

export const LOGOUT = 'LOGOUT';

export function logout() {
  return function (dispatch) {
    dispatch({ type: LOGOUT });
  };
}

export function createAccount(account) {
  return function (dispatch) {
    dispatch({ type: CREATE_ACCOUNT__REQUEST });
    request('/create-account', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(account),
    })
      .then((data) => dispatch({ type: CREATE_ACCOUNT__SUCCESS, payload: data }))
      .catch((err) => dispatch({ type: CREATE_ACCOUNT__FAILURE, payload: err }));
  };
}

export function login(account) {
  return function (dispatch) {
    dispatch({ type: LOGIN__REQUEST });
    request('/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(account),
    })
      .then((data) => dispatch({ type: LOGIN__SUCCESS, payload: data }))
      .catch((err) => dispatch({ type: LOGIN__FAILURE, payload: err }));
  };
}

export function exitSignup() {
  return function (dispatch) {
    dispatch({ type: EXIT_SIGNUP });
  };
}

export function exitLogin() {
  return function (dispatch) {
    dispatch({ type: EXIT_LOGIN });
  };
}

export function fetchMainAccount(username) {
  return function (dispatch) {
    dispatch({ type: FETCH_ACCOUNT__REQUEST });
    request(`/account/${username}`)
      .then((data) => dispatch({ type: FETCH_ACCOUNT__SUCCESS, payload: data }))
      .catch((err) => dispatch({ type: FETCH_ACCOUNT__FAILURE, payload: err }));
  };
}

export function addViewedPost(username, artId) {
  return function (dispatch) {
    dispatch({ type: ADD_VIEWS__REQUEST });
    console.log('Request ID', artId);
    request(`/account/${username}/viewedPosts`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ ID: artId }),
    })
      .then((data) => {
        dispatch({ type: ADD_VIEWS__SUCCESS, payload: data });
        dispatch(fetchMainAccount(username));
      })
      .catch((err) => {
        dispatch({ type: ADD_VIEWS__FAILURE, payload: err });
        dispatch(fetchMainAccount(username));
      });
  };
}

export function addLikedPost(username, artId) {
  return function (dispatch) {
    dispatch({ type: ADD_LIKE__REQUEST });
    return request(`/account/${username}/likedPosts`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ ID: artId }),
    })
      .then((data) => {
        dispatch({ type: ADD_LIKE__SUCCESS, payload: data });
        return Promise.resolve(data);
      })
      .catch((err) => {
        dispatch({ type: ADD_LIKE__FAILURE, payload: err });
        return Promise.reject(err);
      });
  };
}

export function removeLikedPost(username, artId) {
  return function (dispatch) {
    dispatch({ type: REMOVE_LIKE__REQUEST });
    return request(`/account/${username}/likedPosts/${artId}`, {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
    })
      .then((data) => {
        dispatch({ type: REMOVE_LIKE__SUCCESS, payload: data });
        return Promise.resolve(data); // Resolve the promise with data
      })
      .catch((err) => {
        console.log(err);
        dispatch({ type: REMOVE_LIKE__FAILURE, payload: err });
        return Promise.reject(err); // Reject the promise with error
      });
  };
}
