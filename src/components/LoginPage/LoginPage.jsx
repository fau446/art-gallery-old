import React, { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import styles from './LoginPage.module.css';
import { login, exitLogin } from '../../services/actions/account';

function LoginPage({ logIn, setUser }) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { loggedIn, error } = useSelector((state) => state.accountStore);

  async function submitForm(e) {
    e.preventDefault();
    dispatch(login({ username: e.target[0].value, password: e.target[1].value }));
    setUser(e.target[0].value);
  }

  // eslint-disable-next-line consistent-return
  useEffect(() => {
    // This effect runs when `loggedIn` changes.
    console.log('LoginPage.jsx: useEffect');
    console.log('loggedIn:', loggedIn);
    if (loggedIn) {
      // You can display a modal or perform other actions here.
      const timer = setTimeout(() => {
        dispatch(exitLogin());
        logIn();
        navigate('/');
      }, 1000);

      // Clean up the timer when the component unmounts or `loggedIn` changes again.
      return () => clearTimeout(timer);
    }
  }, [loggedIn, dispatch, logIn, navigate]);

  return (
    <div className={styles.background}>
      <div className={styles.login}>
        {error && (
          <p className={styles.error}>Username or password is incorrect. Please try again.</p>
        )}
        {loggedIn && (
          <p className={styles.success}>Login Successful!</p>
        )}
        <h1>Log in to Art Gallery</h1>
        <form onSubmit={submitForm}>
          <label htmlFor="username">
            Username:
            <input type="text" id="username" name="username" required />
          </label>
          <label htmlFor="password">
            Password:
            <input type="password" id="password" name="password" required />
          </label>
          <input className={styles.submit} type="submit" value="Log in" />
        </form>
        <Link to="/signup"><input className={styles.createbtn} type="submit" value="Create an Account" /></Link>
        <Link to="/"><input className={styles.homebtn} type="submit" value="Back to Homepage" /></Link>
      </div>
    </div>
  );
}

export default LoginPage;
