import React from 'react';
import { useNavigate } from 'react-router-dom';
import styles from './Comment.module.css';
import { fetchAccount } from '../../utils/api';

function Comment({ username, comment, profilePic }) {
  // currently using fake data
  const navigate = useNavigate();

  const onClick = async () => {
    const account = await fetchAccount(username);
    navigate('/userpage', { state: { account } });
  };

  // otherUserData being passed through <Link>
  return (
    <div className={styles.comment}>
      <div className={styles.profilepic}>
        {/* eslint-disable-next-line max-len */}
        {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
        <div onClick={onClick}>
          <img className={styles.avatar} src={profilePic} alt="Profile" />
        </div>
      </div>
      <div className={styles.content}>
        <h3>{username}</h3>
        <p className={styles.p}>{comment}</p>
      </div>
    </div>
  );
}

export default Comment;
