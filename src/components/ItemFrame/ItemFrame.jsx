import React from 'react';
import styles from './ItemFrame.module.css';

function ItemFrame({
  title, author, views, likes, numOfComments, imageURL, openModal, handleKeyPress, id, artID,
}) {
  return (
    <div className={styles.item}>
      <div className={styles.content}>
        <h2>{title}</h2>
        <p className={styles.p}>
          Author:
          {' '}
          {author}
        </p>
        <div className={styles.upload_stats}>
          <p className={styles.p}>
            Likes:
            {' '}
            {likes}
          </p>
          <p className={styles.p}>
            Comments:
            {' '}
            {numOfComments}
          </p>
          <p className={styles.p}>
            Views:
            {' '}
            {views}
          </p>
        </div>
      </div>
      <img
        src={imageURL}
        alt="Post Pic"
        data-id={id}
        onClick={(e) => openModal(e, artID)}
        onKeyDown={handleKeyPress}
        className={styles.img}
      />

    </div>
  );
}

export default ItemFrame;
