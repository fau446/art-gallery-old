import React, { useState } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import MainPage from './MainPage';
import LoginPage from './components/LoginPage/LoginPage';
import SignupPage from './components/SignupPage/SignupPage';
import UserPage from './components/UserPage/UserPage';
import OtherUserPage from './components/OtherUserPage/OtherUserPage';
import { logout } from './services/actions/account';

function Router() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [loggedInUser, setLoggedInUser] = useState('');
  const dispatch = useDispatch();

  function logIn() {
    setIsLoggedIn(true);
  }

  function logOut() {
    dispatch(logout());
    setIsLoggedIn(false);
  }

  function setUser(username) {
    setLoggedInUser(username);
  }

  const router = createBrowserRouter([
    {
      path: '/',
      element: <MainPage isLoggedIn={isLoggedIn} logOut={logOut} loggedInUser={loggedInUser} />,
    },
    {
      path: '/login',
      element: <LoginPage logIn={logIn} setUser={setUser} />,
    },
    {
      path: '/signup',
      element: <SignupPage logIn={logIn} setUser={setUser} />,
    },
    {
      path: '/userpage',
      element: <UserPage
        isLoggedIn={isLoggedIn}
        loggedInUser={loggedInUser}
      />,
    },
    {
      path: '/otheruserpage',
      element: <OtherUserPage
        isLoggedIn={isLoggedIn}
        loggedInUser={loggedInUser}
      />,
    },
  ]);

  return <RouterProvider router={router} />;
}

export default Router;
